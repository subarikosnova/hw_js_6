async function findIP() {
    try {
        // Get client's IP address using ipify.org
        const ipifyResponse = await fetch('https://api.ipify.org/?format=json');
        const ipifyData = await ipifyResponse.json();

        const clientIP = ipifyData.ip;

        // Get information about the physical address using ip-api.com
        const ipApiResponse = await fetch(`http://ip-api.com/json/${clientIP}?fields=status,message,continent,continentCode,country,countryCode,region,regionName,city,district,zip,lat,lon,timezone,offset,currency,isp,org,as,query`);
        const ipApiData = await ipApiResponse.json();

        // Display information on the page
        const resultDiv = document.getElementById('result');
        resultDiv.innerHTML = `
            <p><strong>Continent:</strong> ${ipApiData.continent}</p>
            <p><strong>Country:</strong> ${ipApiData.country}</p>
            <p><strong>Region:</strong> ${ipApiData.regionName}</p>
            <p><strong>City:</strong> ${ipApiData.city}</p>
            <p><strong>District:</strong> Цей сайт не знаходить район</p>
        `;
    } catch (error) {
        console.error('Error:', error);
    }
}


